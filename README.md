# renovate

Repository to recreate https://github.com/renovatebot/renovate/issues/14602

Assumption: Renovate cannot determine the version of the modules in https://gitlab.com/renovate-issue-14602/mymodule/-/infrastructure_registry
because it queries https://gitlab.com/api/v4/packages/terraform/modules/v1/renovate-issue-14602/mymodule/local/ instead of https://gitlab.com/api/v4/packages/terraform/modules/v1/renovate-issue-14602/mymodule/local/versions

This repo has a basic gitlab pipeline to run renovate, trying to update the terraform code in https://gitlab.com/renovate-issue-14602/update-this
